# mikeos


## Commands 

````
cd source/bootload 
nasm -O0 -w+orphan-labels -f bin -o bootload.bin bootload.asm || exit
````


````
echo ">>> Assembling MikeOS kernel..."
cd source
nasm -O0 -w+orphan-labels -f bin -o kernel.bin kernel.asm 
````
(it will use dir features/)



## Ready to use

cat file.memstick.img > /dev/sdX



## Compilation on a Raspberry pi for x86


Build with nasm, C, and genisoimage:

````
  sh compile-kernel.sh 
````


## Media

![](media/mikeos-1.png)

#!/bin/sh

# This script assembles the MikeOS bootloader, kernel and programs
# with NASM, and then creates floppy and CD images (on Linux)

# Only the root user can mount the floppy disk image as a virtual
# drive (loopback mounting), in order to copy across the files

# (If you need to blank the floppy image: 'mkdosfs disk_images/mikeos.flp')


if test "`whoami`" != "root" ; then
	echo "You must be logged in as root to build (for loopback mounting)"
	echo "Enter 'su' or 'sudo bash' to switch to root"
	exit
fi


if [ ! -e disk_images/mikeos.flp ]
then
	echo ">>> Creating new MikeOS floppy image..."
	mkdosfs -C disk_images/mikeos.flp 1440 || exit
fi


echo ">>> Assembling bootloader..."
echo COMMAND  nasm -O0 -w+orphan-labels -f bin -o source/bootload/bootload.bin source/bootload/bootload.asm 
nasm -O0 -w+orphan-labels -f bin -o source/bootload/bootload.bin source/bootload/bootload.asm || exit



echo ">>> Assembling MikeOS kernel..."
cd source
echo COMMAND nasm -O0 -w+orphan-labels -f bin -o kernel.bin kernel.asm 
nasm -O0 -w+orphan-labels -f bin -o kernel.bin kernel.asm || exit
cd ..




echo ">>> Assembling programs..."
cd programs
for i in *.asm
do
	echo COMMAND nasm -O0 -w+orphan-labels -f bin $i -o `basename $i .asm`.bin 
	nasm -O0 -w+orphan-labels -f bin $i -o `basename $i .asm`.bin || exit
done



cd ..
echo ">>> Adding bootloader to floppy image..."
echo COMMAND dd status=noxfer conv=notrunc if=source/bootload/bootload.bin of=disk_images/mikeos.flp 
dd status=noxfer conv=notrunc if=source/bootload/bootload.bin of=disk_images/mikeos.flp || exit


echo ">>> Copying MikeOS kernel and programs..."
rm -rf tmp-loop
mkdir tmp-loop && mount -o loop -t vfat disk_images/mikeos.flp tmp-loop && cp source/kernel.bin tmp-loop/
cp programs/*.bin programs/*.bas programs/sample.pcx tmp-loop

sync 
sleep 0.2

echo ">>> Unmounting loopback floppy..."

umount tmp-loop || exit

rm -rf tmp-loop


echo ">>> Creating CD-ROM ISO image..."

rm -f disk_images/mikeos.iso
## echo mkisofs -quiet -V 'MIKEOS' -input-charset iso8859-1 -o disk_images/mikeos.iso -b mikeos.flp disk_images/ || exit

   pwd 
   echo COMMAND genisoimage  -quiet -V 'MIKEOS' -input-charset iso8859-1 -o disk_images/mikeos.iso -b mikeos.flp disk_images/

   genisoimage  -quiet -V 'MIKEOS' -input-charset iso8859-1 -o disk_images/mikeos.iso -b mikeos.flp disk_images/

   ls -ltra disk_images/mikeos.iso 


echo '>>> Done!'
exit






